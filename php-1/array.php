<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3>Soal 1</h3>";
    /*
        SOAL NO 1
        Kelompokkan nama-nama dibawah ini ke dalam Array
        Kids: "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"
        Adults: "Hopper", "Nancy", "Joyce", "Jonathan", "Murray"
    */
    $kids = ['Mike', 'Dustin', 'Will', 'Lucas', 'Max', 'Eleven'];
    $adults = ['Hopper', 'Nancy', 'Joyce', 'Jonathan', 'Murray'];

    print_r($kids);
    echo "<br>";
    print_r($adults);

    echo "<h3>Soal 2</h3>";
    /*
        SOAL NO 2
        Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
    */
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: "; // Berapa panjang array kids
    echo count($kids);
    echo "<br>";
    echo "<ol>";
    for ($i=0; $i < count($kids); $i++) { 
        echo "<li>" . $kids[$i] . "</li>";
    }
    echo "</ol>";

    echo "Total Adults: "; // Berapa panjang array adults
    echo count($adults);
    echo "<br>";
    echo "<ol>";
    for ($i=0; $i < count($adults); $i++) { 
        echo "<li>" . $adults[$i] . "</li>";
    }
    echo "</ol>";

    /*
        SOAL No 3
        Susun data-data berikut ke dalam bentuk Assosiatif Array didalam Array Multidimensi

        Name: "Will Byers"
        Age: 12,
        Aliases: "Will the Wise"
        Status: "Alive"

        Name: "Mike Wheeler"
        Age: 12,
        Aliases: "Dungeon Master"
        Status: "Alive"

        Name: "Jim Hopper"
        Age: 43,
        Aliases: "Chief Hopper"
        Status: "Deceased"

        Name: "Eleven"
        Age: 12,
        Aliases: "El"
        Status: "Alive"
    */

    $data = array(
        array(
            array(
                'Name' => 'Will Byers',
                'Age' => 12,
                'Aliases' => 'Will the Wise',
                'Status' => 'Alive'
            ),
            array(
                'Name' => 'Mike Wheeler',
                'Age' => 12,
                'Aliases' => 'Dungeon Master',
                'Status' => 'Alive'
            ),
            array(
                'Name' => 'Jim Hopper',
                'Age' => 43,
                'Aliases' => 'Chief Hopper',
                'Status' => 'Deceased'
            ),
            array(
                'Name' => 'Eleven',
                'Age' => 12,
                'Aliases' => 'El',
                'Status' => 'Alive'
            ),
        )
    );

    for ($i=0; $i < count($data); $i++) { 
        print_r($data[$i]);
    }
    ?>
</body>
</html>