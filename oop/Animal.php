<?php

class Animal {
    public $legs;
    public $cold_blooded;
    public $name;
    public function __construct($name) {
        $legs = 4;
        $cold_blooded = "no";
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
}